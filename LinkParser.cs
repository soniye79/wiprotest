using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text.RegularExpressions;

namespace WiproTest.Crawler
{
    public class LinkParser
    {
        public LinkParser() { }

        private const string _LINK_REGEX = "href=\"[a-zA-Z./:&\\d_-]+\"";

        private List<string> _goodUrls = new List<string>();
        private List<string> _badUrls = new List<string>();
        private List<string> _otherUrls = new List<string>();
        private List<string> _externalUrls = new List<string>();
        private List<string> _exceptions = new List<string>();
        
        public List<string> GoodUrls
        {
            get { return _goodUrls; }
            set { _goodUrls = value; }
        }

        public List<string> BadUrls
        {
            get { return _badUrls; }
            set { _badUrls = value; }
        }

        public List<string> OtherUrls
        {
            get { return _otherUrls; }
            set { _otherUrls = value; }
        }

        public List<string> ExternalUrls
        {
            get { return _externalUrls; }
            set { _externalUrls = value; }
        }

        public List<string> Exceptions
        {
            get { return _exceptions; }
            set { _exceptions = value; }
        }

        public void ParseLinks(Page page, string sourceUrl)
        {
            MatchCollection matches = Regex.Matches(page.Text, _LINK_REGEX);

            for (int i = 0; i <= matches.Count - 1; i++)
            {
                Match anchorMatch = matches[i];

                if (anchorMatch.Value == String.Empty)
                {
                    BadUrls.Add("Blank url value on page " + sourceUrl);
                    continue;
                }

                string foundHref = null;
                try
                {
                    foundHref = anchorMatch.Value.Replace("href=\"", "");
                    foundHref = foundHref.Substring(0, foundHref.IndexOf("\""));
                }
                catch (Exception exc)
                {
                    Exceptions.Add("Error parsing matched href: " + exc.Message);
                }


                if (!GoodUrls.Contains(foundHref))
                {
                    if (IsExternalUrl(foundHref))
                    {
                        _externalUrls.Add(foundHref);
                    }
                    else if (!IsAWebPage(foundHref))
                    {
                        foundHref = Crawler.FixPath(sourceUrl, foundHref);
                        _otherUrls.Add(foundHref);
                    }
                    else
                    {
                        GoodUrls.Add(foundHref);
                    }
                }
            }
        }


        private static bool IsExternalUrl(string url)
        {
            if (url.IndexOf(ConfigurationManager.AppSettings["authority"]) > -1)
            {
                return false;
            }
            else if 
                ( (url.Length >= 7 && url.Substring(0, 7) == "http://") || ( url.Length >= 3 &&url.Substring(0, 3) == "www") 
                  || ( url.Length >= 7 && url.Substring(0, 7) == "https://")
                )
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Is the value of the href pointing to a web page?
        /// </summary>
        private static bool IsAWebPage(string foundHref)
        {
            if (foundHref.IndexOf("javascript:") == 0)
                return false;

            string extension = foundHref.Substring(foundHref.LastIndexOf(".") + 1, foundHref.Length - foundHref.LastIndexOf(".") - 1);
            switch (extension)
            {
                case "jpg":
                case "css":
                    return false;
                default:
                    return true;
            }

        }
    }
}
