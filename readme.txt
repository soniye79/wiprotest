CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Design decisions
 


INTRODUCTION
------------

It is a Wipro test exercise that crawls over a url and reports all the links. It is a console application and created using VS2013.
In order to build and run this application, please follow the below steps:

1.extract the WiproTest.rar into a local directory on your PC.
2.now, look for the build.bat file in the WiproTest folder. Open the build file, and look for variable called loc.
3.also check there is a .sln file in the WiproTest folder where you found the build.bat.
4.open build.bat file by right clicking and choosing edit. Don't open by double clicking as that will execute the file.
5.replace the loc variable with the location of the WiproTestCrawler solution file (.sln). save and close the build.bat file.
6.Now, open the DOS prompt and type command 
    cd location of the build.bat file.
   for example cd c:\users\maninder\desktop\wiprotest
7. Now open the app.config file and insert your url in the url field. Please use a small website with few links only.
   There is no intermittent reporting on the progress of the crawler which means it will be a long time before you can see the final results.

   Also, add location of the report.htm file in the app.config file.

8.type command build.bat on the dos prompt in order to build and run your application.
9. Check the report.htm file in the local location on your machine to check results.

Alternatively, open the solution in VS 2013 and run the console application.

DESIGN DECISIONS
----------------
  A tiered design is used however for the sake of simplicity only one project is used. 
  
Some shortcomings:
 -A non TDD approach as most time spent in building the code to parse and print the parsing outcome of the crawling.

- No comment headers for classes, and missing use of style corp to enforce better code coverage.

- Only use small websites with few links since parser doesn't report intemittent results which can lead to a
long wait before report.htm can be checked in your local folder.

- Non threaded model since only 4-5 hours have been spent on it.

- No tests at the moment due to limit time spent on the application.